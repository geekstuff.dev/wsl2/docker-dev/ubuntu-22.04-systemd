#!make

SOURCE_IMAGE ?= ubuntu
SOURCE_TAG ?= 22.04
TITLE = Docker Dev / Ubuntu 22.04 systemd
SYSD_VARIANT = systemd

# used in devcontainer
-include /workspace/common/dev-setup/custom-image.mk

# used in gitlab-ci
ifeq ($(MAKE_LIB),)
include dev-setup/custom-image.mk
endif
