ARG SOURCE_IMAGE
ARG SOURCE_TAG

FROM ${SOURCE_IMAGE}:${SOURCE_TAG}

ARG BUILD_REF
ARG BUILD_SOURCE
ARG DOCKER_DEV_SERVICES="docker, gpg-agent, ssh-agent"
ARG SOURCE_IMAGE
ARG SOURCE_TAG

ENV BUILT_FROM="${SOURCE_IMAGE}:${SOURCE_TAG}"
ENV BUILD_REF=${BUILD_REF}
ENV BUILD_SOURCE=${BUILD_SOURCE}
ENV SYSD_VARIANT="systemd"
ENV WSL_USER="wsluser"

# Copy wsl2/common/dev-setup, install minimal requirements and docker-dev bin
#   https://gitlab.com/geekstuff.dev/wsl2/common/dev-setup
COPY ./dev-setup/ /usr/lib/docker-dev/
RUN /usr/lib/docker-dev/bin/init.sh

# Create docker-dev WSL image
RUN ["docker-dev", "00-basics"]
RUN ["docker-dev", "10-template-user"]
RUN ["docker-dev", "20-docker"]
RUN ["docker-dev", "30-wsl-conf"]
RUN ["docker-dev", "40-user-config"]
RUN ["docker-dev", "50-wsl-services"]
RUN ["docker-dev", "99-post-import"]
